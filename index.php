<!doctype html>
<html>
<head>
	<title>Presentations by Ashish Shah</title>
	<link href="index.css" type="text/css" rel="stylesheet" />
</head>
<body>
	<?php
		function startsWith($haystack, $needle) {
    			// search backwards starting from haystack length characters from the end
    			return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
		}
		function endsWith($haystack, $needle) {
			// search forward starting from end minus needle length characters
			return $needle === "" || strpos($haystack, $needle, strlen($haystack) - strlen($needle)) !== FALSE;
		}
	?>
	<div class="pres_ctr">
		<?php
			$dir = '.';
			foreach ( scandir( $dir ) as $file ) {
				if( endsWith( $file, '.htm') || endsWith( $file, '.html') ) {
					print_r( "<a class=\"pres_name\" href=" . $file . ">" . $file . "</a>\n");
				}
			}
		?>
	</div>
</body>
</html>
