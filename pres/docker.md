# What is Docker?
test

# Docker Architecture


# Linux Containers
Operating-system-level virtualization environment for

running multiple isolated Linux systems (containers)

on a single Linux control host

* libvirt
* |--+--------------------|
* |-----|--------|--------|
* xen, lxc, virtualbox, vmware
* cgroups [Wiki](http://en.wikipedia.org/wiki/Cgroups)
  * Resource Limitation
  * Prioritization
  * Accounting
  * Control



# Why not Virtualization?
* Binary translation
* Traps
* Emulation
* Hybrid Virtualization
  * Virtualization drivers calling the API of the hypervisor



# Docker architecture
  * Daemon
  * Client


# Integration
Amazon Web Services, Ansible, CFEngine, Chef, Google Cloud Platform,
Jenkins, Microsoft Azure, OpenStack Nova, OpenSVC, Puppet,
Salt and Vagrant


# Docker images
* Managing
* Location
* Creation
* Consuming
* [Link](https://docs.docker.com/userguide/dockerimages/)


# Docker containers
* [Link](https://docs.docker.com/userguide/dockercontainers/)


# Docker volumes
* [Link](https://docs.docker.com/userguide/dockervolumes/)

# Docker Commands
* Listing containers
* Start/Stop/Killing containers
* Removing containers
* `docker pull`
* `docker run -p 80:80 -m [] tutum/wordpress`



# Dockerfile
  * Series of commands to create an image
  * Command organization order - pull/init/start/scripts etc.
  * Commands


  |Command|Meaning|
  |:------|:------|
  |`FROM`||
  |`MAINTAINER`||
  |`VOLUME`||
  |`ENTRYPOINT`||
  |`WORKDIR`||
  |`ENV`||
  |`EXPOSE`||
  |`ADD`||
  |`USER`||
  |`RUN`|test|
  |`CMD`|command stuff|


# Dockerizing applications
* Dockerfile
* [Link](https://docs.docker.com/userguide/dockerizing/)


# How to deploy/CI
* Whole images

# How it fits in the current "CloudScape"



# Docker tools
* Ansible
  * Ansible Galaxy
  * Ansible Role Manager
  * [consul-ansible](https://github.com/jivesoftware/ansible-consul)
* Project Atomic
* Consul
* CoreOS
  * systemd
  * etcd
  * fleetctl
* CoreOS Rocket
* Panamax
* fig.sh
  * http://www.fig.sh/django.html
* Dockerhub
  * https://github.com/docker/docker-registry
  * [Wordpress](https://registry.hub.docker.com/_/wordpress/)
  * [CentOS](https://registry.hub.docker.com/_/centos/)
* boot2docker
* Kubernetes
* Docker + YARN + Kubernetes [Paper](http://hortonworks.com/blog/docker-kubernetes-apache-hadoop-yarn/)
* SaltStack
* Capistrano
* [decking.io](decking.io)
* [docker tools](http://blog.ricardoamaro.com/content/docker-tools-help-containers-conquer-server-world)
* [make for docker](http://marmelab.com/blog/2014/09/10/make-docker-command.html)
* [docker tips](http://www.projectatomic.io/docs/docker-image-author-guidance/)
* Helios
* ZooKeeper
* Mesos
* [dockerfile repo](https://dockerfile.github.io/#/python)
* snakebite
* CircleCI
* Shippable
* supervisor
* terraform
* Packer



# Use cases
* Hadoop
  * http://hortonworks.com/hadoop-tutorial/lap-around-deploying-managing-configuring-hdp-ambari-1-7/
  * http://blog.sequenceiq.com/blog/2014/06/17/ambari-cluster-on-docker/
  * http://hortonworks.com/blog/go-hadoop-err-hadoop-and-go/
  * http://hortonworks.com/blog/docker-kubernetes-apache-hadoop-yarn/


# Best Practices
https://docs.docker.com/articles/dockerfile_best-practices/



# Example usage
* sprint_utils
* host-loyalty
* hadoop on CoreOS
  * Running an MR job on hadoop
* Running WP on same host



# When not to use Docker



# Getting Help
* IRC - freenode #docker
* Github
* Twitter


# Further reading
* [](http://www.slideshare.net/JanosMatyas/docker-based-hadoop-provisioning)
* [](http://sequenceiq.com/cloudbreak/)
  * figures and data


# Thank you
Ashish Shah
