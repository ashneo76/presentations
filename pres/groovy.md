# Groovy
# Features
* `#!` only on the first line
* Comments
* Comparing Java
  * Features added by Groovy
  * Brevity
  * Auto-imported packages in Groovy

# Source changes
* Classes
  * Example
* GroovyBeans
* GStrings
  * Single/Double quotes
  * Interpolation

* GDK

# Installation/GVM
# Maven Linking
# Java 8
# Closures
# Lambdas
# Scripting
* Convert sprint_perf to groovy
* Compare with other scripting languages


# Gradle
# Use cases
# When not to use

https://gradle.org/docs/current/userguide/tutorial_gradle_command_line.html
http://griffon.codehaus.org/
